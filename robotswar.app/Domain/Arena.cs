﻿using System.Collections.Generic;

namespace robotswar.app.Domain
{
    public class Arena
    {
        public Position Limits
        {
            get;
            private set;
        }

        public List<Robot> Robots
        {
            get;
            private set;
        }

        public Arena(Position limits)
        {
            this.Limits = limits;

            this.Robots = new List<Robot>();
        }

        public void Add(Robot robot)
        {
            this.Robots.Add(robot);

            robot.SetLimits(Limits);
        }
    }
}
