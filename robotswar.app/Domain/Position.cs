﻿namespace robotswar.app.Domain
{
    public class Position
    {
        public int X;
        public int Y;

        public Position(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public override string ToString()
        {
            return X + " " + Y;
        }
    }
}