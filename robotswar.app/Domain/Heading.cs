﻿namespace robotswar.app.Domain
{
    public enum Heading
    {
        North,
        East,
        South,
        West
    }
}