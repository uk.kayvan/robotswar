﻿namespace robotswar.app.Domain
{
    public class Robot
    {
        private Position limit;
        private Navigator navigator;

        public Position CurrentPosition
        {
            get;
            private set;
        }

        public Heading CurrentHeading
        {
            get;
            private set;
        }


        public Robot(int x, int y, Heading heading)
        {
            this.navigator = new Navigator();
            this.CurrentPosition = new Position(x, y);
            this.CurrentHeading = heading;
        }

        public void Move()
        {
            if (HasReachedLimits())
            {
                return;
            }

            this.navigator.FincNextLoc(CurrentPosition, CurrentHeading);
        }

        public void TurnLeft()
        {
            CurrentHeading = this.navigator.GetLeftDirection(CurrentHeading);
        }

        public void TurnRight()
        {
            CurrentHeading = this.navigator.GetRightDirection(CurrentHeading);
        }

        public void SetLimits(Position posit)
        {
            this.limit = posit;
        }

        private bool HasReachedLimits()
        {
            return
                (CurrentHeading == Heading.North && CurrentPosition.Y == limit.Y) ||
                (CurrentHeading == Heading.South && CurrentPosition.Y == 0) ||
                (CurrentHeading == Heading.East && CurrentPosition.X == limit.X) ||
                (CurrentHeading == Heading.West && CurrentPosition.Y == 0);
        }

    }
}
