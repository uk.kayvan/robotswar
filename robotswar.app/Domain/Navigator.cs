﻿namespace robotswar.app.Domain
{
    public class Navigator
    {
        public Position FincNextLoc(Position position, Heading heading)
        {
            switch (heading)
            {
                case Heading.North: position.Y++; break;
                case Heading.South: position.Y--; break;
                case Heading.West: position.X--; break;

                default: position.X++; break;
            }

            return position;
        }

        public Heading GetLeftDirection(Heading heading)
        {
            switch (heading)
            {
                case Heading.North: return Heading.West;
                case Heading.South: return Heading.East;
                case Heading.West: return Heading.South;

                default: return Heading.North;
            }
        }

        public Heading GetRightDirection(Heading heading)
        {
            switch (heading)
            {
                case Heading.North: return Heading.East;
                case Heading.South: return Heading.West;
                case Heading.West: return Heading.North;

                default: return Heading.South;
            }
        }

    }
}
