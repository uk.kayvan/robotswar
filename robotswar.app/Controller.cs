﻿using System;
using robotswar.app.Domain;

namespace robotswar.app
{
    public class Controller
    {
        // TODO: use InputParser to parse input strings into objects/commands

        public Arena Arena
        {
            get;
            private set;
        }

        public void Run()
        {
            ReadArena();

            ReadRobots();

            MoveRobots();

            PrintOutputs();
        }

        private void PrintOutputs()
        {
            throw new NotImplementedException();
        }

        private void MoveRobots()
        {
            throw new NotImplementedException();
        }

        private void ReadArena()
        {
            throw new NotImplementedException();
        }

        private void ReadRobots()
        {
            throw new NotImplementedException();
        }
    }
}
