﻿using System;

namespace robotswar.app
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Robots war started ..");

            var controller = new Controller();

            controller.Run();
        }
    }
}
