# RobotsWar

RobotsWar is a simple console app that accepts inputs to set up an arena, place robots and start moving them one by one.

#### Assumptions
DOTNET Core 2.1 and CLI are installed


## Instructions
#### 1. Download and build the project
```python
 $ cd robotswar
 $ dotnet build
```

#### 2. Running tests
```python
 $ cd robotswar.test
 $ dotnet test
```

### Notes:
* This project is not completed although domain model is pretty much implemented and tested.
* InputParser and Controller classes should be implemented and tested. Parser to take care of parsing user input into commands and objects, and Controller to take case of orchestrating the actions and running the game. Program class only invokes the Controller and doesn't do more. Classes are kept simple and not over-engineered in line with current requirements.
* Extending: 
1. Add DI container - Can bootstrap the container using dotnet core built in DI. For instance, Console should be (wrapped and) injected as a dependency into InputParser for testability.
2. Exception handling and input validation should be added in InputParser, Controller and event domain classes. 
3. Domain and app classes could be moved into a class lib project and referenced in the console (runner process).

