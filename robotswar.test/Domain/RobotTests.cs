using robotswar.app.Domain;
using Xunit;

namespace robotswar.test
{
    public class RobotTests
    {

        [Fact]
        public void testRobotMoveNorth()
        {

            Arena arena = new Arena(new Position(6, 6));
            Robot robot1 = new Robot(2, 2, Heading.North);
            arena.Add(robot1);

            robot1.Move();

            Assert.True(robot1.CurrentPosition.Y == 3);
        }

        [Fact]
        public void testRobotMoveSouth()
        {
            Arena arena = new Arena(new Position(6, 6));
            Robot robot1 = new Robot(2, 2, Heading.South);
            arena.Add(robot1);

            robot1.Move();

            Assert.True(robot1.CurrentPosition.Y == 1);
        }

        [Fact]
        public void testRobotMoveEast()
        {
            Arena arena = new Arena(new Position(6, 6));
            Robot robot1 = new Robot(2, 2, Heading.East);
            arena.Add(robot1);

            robot1.Move();

            Assert.True(robot1.CurrentPosition.X == 3);
        }

        [Fact]
        public void testRobotMoveWest()
        {

            Arena arena = new Arena(new Position(6, 6));
            Robot robot1 = new Robot(2, 2, Heading.West);
            arena.Add(robot1);

            robot1.Move();

            Assert.True(robot1.CurrentPosition.X == 1);
        }


        [Theory]
        [InlineData(Heading.North, Heading.West)]
        [InlineData(Heading.West, Heading.South)]
        [InlineData(Heading.South, Heading.East)]
        [InlineData(Heading.East, Heading.North)]
        public void testRobotTurnsLeft(Heading currentHeading, Heading expectedHeading)
        {

            Arena arena = new Arena(new Position(6, 6));
            Robot robot1 = new Robot(2, 2, currentHeading);
            arena.Add(robot1);

            robot1.TurnLeft();

            Assert.True(robot1.CurrentHeading == expectedHeading);
        }


        [Theory]
        [InlineData(Heading.North, Heading.East)]
        [InlineData(Heading.East, Heading.South)]
        [InlineData(Heading.South, Heading.West)]
        [InlineData(Heading.West, Heading.North)]
        public void testRobotTurnsRight(Heading currentHeading, Heading expectedHeading)
        {

            Arena arena = new Arena(new Position(6, 6));
            Robot robot1 = new Robot(2, 2, currentHeading);
            arena.Add(robot1);

            robot1.TurnRight();

            Assert.True(robot1.CurrentHeading == expectedHeading);
        }

        [Fact]
        public void testRobotDoesntCrossLimits()
        {
            Arena arena = new Arena(new Position(3, 3));
            Robot robot1 = new Robot(3, 3, Heading.North);
            arena.Add(robot1);

            robot1.Move();

            Assert.True(robot1.CurrentPosition.X == 3 && robot1.CurrentPosition.Y == 3);
        }
    }
}
