﻿using robotswar.app.Domain;
using Xunit;

namespace robotswar.test.Domain
{
    public class ArenaTests
    {

        // TODO: test handles invalid Arena size


        // TODO: test handles invalid no of robots


        [Fact]
        public void testSetupArena()
        {
            Arena arena = new Arena(new Position(6, 6));

            Robot robot1 = new Robot(2, 2, Heading.East);
            Robot robot2 = new Robot(4, 4, Heading.South);

            arena.Add(robot1);
            arena.Add(robot2);

            Assert.True(arena.Robots.Count == 2);
            Assert.True(robot1.CurrentPosition.X == 2 && robot1.CurrentPosition.Y == 2 && robot1.CurrentHeading == Heading.East);
            Assert.True(robot2.CurrentPosition.X == 4 && robot2.CurrentPosition.Y == 4 && robot2.CurrentHeading == Heading.South);
        }


    }
}
