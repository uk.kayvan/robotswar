﻿using robotswar.app.Domain;
using Xunit;

namespace robotswar.test.Domain
{
    public class NavigatorTests
    {

        [Theory]
        [InlineData(Heading.North, Heading.West)]
        [InlineData(Heading.West, Heading.South)]
        [InlineData(Heading.South, Heading.East)]
        [InlineData(Heading.East, Heading.North)]
        public void testGetsLeftDirection(Heading current, Heading expected)
        {
            Navigator nav = new Navigator();

            var dir = nav.GetLeftDirection(current);

            Assert.True(dir == expected);
        }

        [Theory]
        [InlineData(Heading.North, Heading.East)]
        [InlineData(Heading.East, Heading.South)]
        [InlineData(Heading.South, Heading.West)]
        [InlineData(Heading.West, Heading.North)]
        public void testGetsRightDirection(Heading current, Heading expected)
        {
            Navigator nav = new Navigator();

            var dir = nav.GetRightDirection(current);

            Assert.True(dir == expected);
        }

        [Fact]
        public void testNavigatesNorth()
        {
            Navigator nav = new Navigator();

            var position = new Position(2, 3);

            nav.FincNextLoc(position, Heading.North);

            Assert.True(position.Y == 4);
            Assert.True(position.X == 2);
        }

        [Fact]
        public void testNavigatesSouth()
        {
            Navigator nav = new Navigator();

            var position = new Position(2, 3);

            nav.FincNextLoc(position, Heading.South);

            Assert.True(position.Y == 2);
            Assert.True(position.X == 2);
        }

        [Fact]
        public void testNavigatesWest()
        {
            Navigator nav = new Navigator();

            var position = new Position(2, 3);

            nav.FincNextLoc(position, Heading.West);

            Assert.True(position.Y == 3);
            Assert.True(position.X == 1);
        }

        [Fact]
        public void testNavigatesEast()
        {
            Navigator nav = new Navigator();

            var position = new Position(2, 3);

            nav.FincNextLoc(position, Heading.East);

            Assert.True(position.Y == 3);
            Assert.True(position.X == 3);
        }
    }
}
